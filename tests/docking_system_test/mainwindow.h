#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "support.hpp"
#include "dock_manager.hpp"
#include "tab.hpp"
#include <QMainWindow>
#include <qwidget_cptr.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

  public:
    explicit MainWindow(QWidget* parent = nullptr);
    virtual ~MainWindow();


  private:
    Ui::MainWindow*                     ui;
    spt::qwidget_cptr<ads::DockManager> dockManager;
};

#endif // MAINWINDOW_H
