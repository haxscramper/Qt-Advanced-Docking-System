# Dev notes

# General outline

+ [Tab](\ref ads::Tab) - container that manages [TabTitle](\ref ads::TabTitle) and [TabWidget](\ref ads::TabWidget) in one piece and manages their lifetime
+ [TabWidget](\ref ads::TabWidget) - container for other widgets. When you set widget for tab it will be added into [TabWidget](\ref ads::TabWidget) layout and old widget will be removed
+ [TabTitle](\ref ads::TabTitle) - widget that will be displayed in title bar. If you constructed [Tab](\ref ads::Tab) with `title` argument as string it will be set to QLabel
+ [FloatingWidget](\ref ads::FloatingWidget) **single** floating widget attached to each [DockManager](\ref ads::DockManager). (Why single? because it will be moved to new window right after you release mouse from it's tab). It will be destroyed immediately after user releases mouse from the tab.

\dotfile dev_notes_lib_stucture.dot Application structure

<DT> Floating widget and new widget creation </DT>

+ Each [DockManager](\ref ads::DockManager) has single `std::unique_ptr` for [FloatingWidget](\ref ads::FloatingWidget) that might or might not be valid at any give time.
+ When user tears tab off the tab is removed from parent [DockWidget](\ref ads::DockWidget) and it's content is transfered to the [DockWidget](\ref ads::DockWidget) inside active floating widget. [TabTitle](\ref ads::TabTitle) gets pointer to newly created floating widget.
+ When user continues to move mouse pressing [TabTitle](\ref ads::TabTitle) this will trigger [moveFloatingWidget](\ref ads::TabTitle::moveFloatingWidget)
+ When user releases mouse from the tab floating widget will be removed from previous [DockManager](\ref ads::DockManager) using [releaseFloating](\ref ads::DockManager::releaseFloating) and put into new manager for [FloatingWindow](\ref ads::FloatingWindow)

\dotfile dev_notes_floating_move.dot Floating widget drag and drop

