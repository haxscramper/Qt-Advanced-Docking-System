#ifndef ADS_API_H
#define ADS_API_H

#include <QFlags>
#include "ads_debug_macro.hpp"

class QWidget;
class QSplitter;

#define ADS_WINDOW_FRAME_BORDER_WIDTH 7


namespace ads {
class DockManager;
class DockWidget;
} // namespace ads

namespace ads {
enum DropArea
{
    InvalidDropArea = 0,
    TopDropArea     = 1,
    RightDropArea   = 2,
    BottomDropArea  = 4,
    LeftDropArea    = 8,
    CenterDropArea  = 16,

    AllAreas = TopDropArea | RightDropArea | BottomDropArea |
               LeftDropArea | CenterDropArea
};
Q_DECLARE_FLAGS(DropAreas, DropArea)

void         deleteEmptySplitter(DockManager* manager);
DockManager* findParentContainerWidget(QWidget* w);
DockWidget*  findParentDockWidget(QWidget* w);
QSplitter*   findParentSplitter(QWidget* w);
QSplitter*   findImmediateSplitter(QWidget* w);

} // namespace ads
#endif
