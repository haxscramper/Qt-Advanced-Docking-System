#ifndef FLOATINGWIDGET_H
#define FLOATINGWIDGET_H

#include <QWidget>
class QBoxLayout;

#include "support.hpp"
#include "tab.hpp"

namespace ads {
class DockManager;
class FloatingWindow;
} // namespace ads


namespace ads {
/*!
 * \brief Temporary widget that will be shown when dragin tabs around.
 */
class FloatingWidget : public QWidget
{
    Q_OBJECT

  public:
    explicit FloatingWidget(DockManager* _manager, std::unique_ptr<Tab> _tab);
    virtual ~FloatingWidget();


  public:
    void                 setFreeFloating(QPoint gpos);
    std::unique_ptr<Tab> releaseContent();
    Tab*                 getTab() const;
    DockManager*         getManager() const;


  private:
    /// Floating window that will be show when widget dockign is finished.
    FloatingWindow* window      = nullptr;
    QBoxLayout*     titleLayout = nullptr;
    DockManager* manager = nullptr; ///< Parent DockManager. Not an owner
    std::unique_ptr<Tab> tab;       ///< Is an owner


    void initUI();
    void setTab(Tab* _tab);
};


} // namespace ads
#endif
