#include "dock_manager.hpp"

#include <QContextMenuEvent>
#include <QDataStream>
#include <QDebug>
#include <QGridLayout>
#include <QMenu>
#include <QPaintEvent>
#include <QPainter>
#include <QPoint>
#include <QSplitter>
#include <QtGlobal>

#include <algorithm/all.hpp>

#include "dock_widget.hpp"
#include "drop_overlay.hpp"
#include "floating_widget.hpp"
#include "tab_title.hpp"
#include "tab_widget.hpp"

std::vector<ads::DockManager*> ads::DockManager::dockManagers;

namespace ads {

static QSplitter* createSplitter(
    Qt::Orientation orientation = Qt::Horizontal,
    QWidget*        parent      = nullptr) {
    QSplitter* splitter = new QSplitter(orientation, parent);
    splitter->setProperty("ads-splitter", QVariant(true));
    splitter->setChildrenCollapsible(false);
    splitter->setOpaqueResize(false);
    return splitter;
}


//  //////////////////////////////////////////////////////////////////////
//  Special member functions
//  //////////////////////////////////////////////////////////////////////

DockManager::DockManager(QWidget* parent)
    : QFrame(parent)
    , floating(std::unique_ptr<FloatingWidget>(nullptr))
    , dropOverlay(new DropOverlay(this))
    , mainLayout(new QGridLayout())
    , _orientation(Qt::Horizontal)

{
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(0);
    mainLayout->addWidget(new QSplitter(this), 0, 0);
    setLayout(mainLayout);
    dockManagers.emplace_back(this);
    static int _ID = 0;
    _ID++;
    ID = _ID;
    ADS_LOG << "Created new dock manager. ID:" << ID;
}

DockManager::~DockManager() {
    ADS_FUNC_BEGIN
    ADS_WARN << "Destroying dock manager";
    spt::erase_all(dockManagers, this);
    ADS_FUNC_END
}

//  //////////////////////////////////////////////////////////////////////
//  Static member functions
//  //////////////////////////////////////////////////////////////////////

void DockManager::hideAllOverlays() {
    std::for_each(
        dockManagers.begin(),
        dockManagers.end(),
        [](DockManager* manager) {
            manager->getDropOverlay()->hideDropOverlay();
        });
}


/*!
 * \brief Return DockManager that is currently under mouse
 * (gometry contains cursor position)
 * \return Pointer to DockManager or nullptr if no mathcing found
 */
DockManager* DockManager::findUnderMouse() {
    return spt::safe_iter_get(
        std::find_if(
            DockManager::dockManagers.begin(),
            DockManager::dockManagers.end(),
            [](DockManager* manager) {
                return manager->geometry().contains(
                    manager->mapFromGlobal(QCursor::pos()));
            }),
        DockManager::dockManagers,
        nullptr);
}


/*!
 * \brief Check whether each manager isEmpty() and if so close() it
 */
void DockManager::closeEmptyManagers() {
    ADS_INFO << "Cleaning up empty managers";
    spt::for_each(dockManagers, [](DockManager* manager) {
        if (manager->isEmpty()) {
            manager->close();
        }
    });
}


//  //////////////////////////////////////////////////////////////////////
//  Public functions
//  //////////////////////////////////////////////////////////////////////

/*!
 * Adds the section-content <em>sc</em> to this container-widget into
 * the section-widget <em>sw</em>. If <em>sw</em> is not nullptr, the
 * <em>area</em> is used to indicate how the content should be
 * arranged. Returns a pointer to the SectionWidget of the added
 * SectionContent. Do not use it for anything else than adding more
 * SectionContent elements with this method.
 * \todo Why this function returns anything? There is no reason to do that
 * \todo What is the point of `DockWidget* sw`? It seems that there is no
 * real use for this parameter.
 */
DockWidget* DockManager::addTab(
    std::unique_ptr<Tab> tab, ///< Content to add
    DockWidget*          sw,  ///< Parent dock manager
    DropArea             area ///< Area to drop widget in
) {
    QObject::connect(
        tab->getTitleWidget(),
        &TabTitle::activeTabChanged,
        this,
        &DockManager::onActiveTabChanged);


    return insertTab(std::move(tab), sw, area);
}

/// \brief Take ownership for new floating widget
void DockManager::setFloating(std::unique_ptr<FloatingWidget> _floating) {
    ADS_INFO << "Setting floating widget";
    ADS_LOG << "Set floating, new floating is valid?"
            << (_floating.get() != nullptr);
    ADS_LOG << "Set floating, old floating is valid?"
            << (floating.get() == nullptr);
    floating.release();
    floating = std::move(_floating);

    ADS_LOG << "Old floating is null?" << (_floating.get() == nullptr);
}

void DockManager::dropFloating(
    std::unique_ptr<FloatingWidget> _floating,
    DockWidget*                     dock,
    DropArea                        area) {
    ADS_FUNC_BEGIN

    _floating->getTab()->setFloating(nullptr);
    ADS_LOG << "Reset floating pointer on tab";

    insertTab(_floating->releaseContent(), dock, area);
    ADS_LOG << "Inserted tab";

    hideAllOverlays();
    ADS_LOG << "Hid overlays";

    ADS_FUNC_END
}


/// \brief Release ownership of floating widget and reset floating widget
/// for this dock manager to nullptr
std::unique_ptr<FloatingWidget> DockManager::releaseFloating() {
    ADS_INFO << "Releasing floating widget";
    auto temp = std::unique_ptr<FloatingWidget>(floating.release());
    ADS_LOG << "Released floating widget. Old widget is null?"
            << (floating.get() == nullptr);
    return temp;
}


/// \brief Delete all empty dock widgets and splitters
void DockManager::deleteEmpty() {
    ADS_FUNC_BEGIN

    ADS_INFO << "Dock count" << docks.size();
    spt::erase_if(docks, [](std::unique_ptr<DockWidget>& dock) -> bool {
        ADS_LOG << "Dock is empty?" << (dock->tabCount() == 0);
        ADS_LOG << "Tab count" << dock->tabCount();
        if (dock->tabCount() == 0) {
            dock->hide();
            dock->deleteLater();
            dock.release();
            ADS_LOG << "Deleting empty dock";
            return true;
        } else {
            ADS_LOG << "Dock is not empty";
            return false;
        }
    });

    ADS_LOG << "Removed empty docks. New count:" << docks.size();


    QList<QSplitter*> splitters = findChildren<QSplitter*>();
    ADS_LOG << "Splitter count:" << splitters.size();

    for (QSplitter* splitter : splitters) {
        for (int i = 0; i < splitter->count(); ++i) {
            QWidget*    widget = splitter->widget(i);
            DockWidget* dock   = qobject_cast<DockWidget*>(widget);
            if (dock != nullptr && dock->tabCount() == 0) {
                dock->hide();
                dock->deleteLater();
            }
        }
        if (splitter->count() == 0) {
            splitter->deleteLater();
        }
    }

    ADS_LOG << "Splitter count:" << splitters.size();

    ADS_FUNC_END
}

void DockManager::debugAll() {
    std::for_each(
        dockManagers.begin(),
        dockManagers.end(),
        [](const DockManager* manager) { manager->logState(); });
}

void DockManager::logState() const {
    ADS_INFO_1 << "Debug all managers";
    ADS_INFO << "Manager ID" << ID;
    DEBUG_INDENT;
    {
        ADS_INFO << "Dock count" << docks.size();
        DEBUG_INDENT
        std::for_each(
            docks.begin(),
            docks.end(),
            [](const std::unique_ptr<DockWidget>& dock) {
                ADS_INFO << "Dock is valid" << (dock.get() != nullptr);
                Q_UNUSED(dock)
            });
        DEBUG_DEINDENT
    }
    ADS_INFO << "Floating is valid" << (floating.get() != nullptr);
    DEBUG_DEINDENT
}

bool DockManager::isEmpty() const {
    return docks.size() == 0;
}

void DockManager::close() {
    if (closeable) {
        QMainWindow* window = qobject_cast<QMainWindow*>(parent());
        DEBUG_ASSERT_TRUE(window != nullptr)
        window->hide();
        window->deleteLater();
        ADS_WARN << "Closing dock widget";
    } else {
#ifdef ADSWIDGET_DEBUG
        DEBUG_WARN_IF_CALLED("Tried to close non-closeable dock manager")
#endif
    }
}


QRect DockManager::outerTopDropRect() const {
    QRect r = rect();
    int   h = r.height() / 100 * 5;
    return QRect(r.left(), r.top(), r.width(), h);
}

QRect DockManager::outerRightDropRect() const {
    QRect r = rect();
    int   w = r.width() / 100 * 5;
    return QRect(r.right() - w, r.top(), w, r.height());
}

QRect DockManager::outerBottomDropRect() const {
    QRect r = rect();
    int   h = r.height() / 100 * 5;
    return QRect(r.left(), r.bottom() - h, r.width(), h);
}

QRect DockManager::outerLeftDropRect() const {
    QRect r = rect();
    int   w = r.width() / 100 * 5;
    return QRect(r.left(), r.top(), w, r.height());
}


std::vector<Tab*> DockManager::getTabs() const {
    ADS_FUNC_BEGIN
    std::vector<Tab*> result;
    for (const std::unique_ptr<DockWidget>& dock : docks) {
        spt::concatenate_copy(
            result, spt::copy_different(dock->getContents(), nullptr));
    }

    ADS_LOG << "Contents count" << result.size();

    ADS_FUNC_END
    return result;
}


//  //////////////////////////////////////////////////////////////////////
//  Private functions
//  //////////////////////////////////////////////////////////////////////


/*!
 * \brief Create new dock widget in this dock manager and return
 * raw pointer to it
 */
DockWidget* DockManager::newDockWidget() {
    docks.push_back(std::make_unique<DockWidget>(this));
    docks.back()->setManager(this);
    return docks.back().get();
}

/*!
 * \brief Drop data into dock widget's DropArea. If necessary create new
 * dock widget
 * \return Dock widget that data was dropped in.
 */
DockWidget* DockManager::insertTab(
    std::unique_ptr<Tab> tab,  ///< Tab to drop into dock widget
    DockWidget*          dock, ///< Dock wdiget to drop data in
    DropArea             area  ///< Area on dock widget to drop data in
) {
    ADS_FUNC_BEGIN


    // Debug
    {
        ADS_LOG << "Tab is valid  " << (tab.get() != nullptr);
        ADS_LOG << "Dock is valid " << (dock != nullptr);

        ADS_INFO_1 << "Dropping into";
        switch (area) {
            case InvalidDropArea:
                ADS_LOG << "Area :" << DS_INFO << "InvalidDropArea";
                break;
            case TopDropArea:
                ADS_LOG << "Area :" << DS_INFO << "TopDropArea";
                break;
            case RightDropArea:
                ADS_LOG << "Area :" << DS_INFO << "RightDropArea";
                break;
            case BottomDropArea:
                ADS_LOG << "Area :" << DS_INFO << "BottomDropArea";
                break;
            case LeftDropArea:
                ADS_LOG << "Area :" << DS_INFO << "LeftDropArea";
                break;
            case CenterDropArea:
                ADS_LOG << "Are :" << DS_INFO << "CenterDropArea";
                break;
            default: break;
        }
    }

    if (dock == nullptr) {
        ADS_FUNC_RET("Dropped into outer area")
        return dropIntoOuterArea(std::move(tab), area);
    } else {
        ADS_FUNC_RET("Dropped into dock widget")
        return dropIntoDock(std::move(tab), dock, area);
    }
}

DockWidget* DockManager::dropIntoOuterArea(
    std::unique_ptr<Tab> tab,
    DropArea             area) {
    ADS_FUNC_BEGIN

    switch (area) {
        case TopDropArea:
            ADS_FUNC_RET("TopDropArea")
            return dropContentOuterHelper(
                std::move(tab), Qt::Vertical, false);
        case RightDropArea:
            ADS_FUNC_RET("RightDropArea")
            return dropContentOuterHelper(
                std::move(tab), Qt::Horizontal, true);
        case CenterDropArea: {
            if (docks.size() == 1) {
                ADS_FUNC_RET("Single dock widget")
                return dropIntoDock(
                    std::move(tab), docks.at(0).get(), area);
            } else if (docks.size() == 0) {
                ADS_FUNC_RET("No dock widgets")
                return dropIntoOuterArea(std::move(tab), TopDropArea);
            } else {
                return nullptr;
            }
        }
            ADS_FUNC_END
        case BottomDropArea:
            ADS_FUNC_RET("")
            return dropContentOuterHelper(
                std::move(tab), Qt::Vertical, true);
        case LeftDropArea:
            ADS_FUNC_RET("LeftDropArea")
            return dropContentOuterHelper(
                std::move(tab), Qt::Horizontal, false);
        default: return nullptr;
    }
}

/*!
 * \brief DockManager::dropIntoDock
 * \note This is a function on which depends positioning, spacing and
 * orientation of dock widgets inserted in non-central area.
 * \todo Make sure that widget is docked in the way that size of others is
 * not affected
 */
DockWidget* DockManager::dropIntoDock(
    std::unique_ptr<Tab> tab,  /// Tab that has to be redocked
    DockWidget*          dock, /// Dock to drop tab into
    DropArea             area  /// Area in the dock to drop tab into
) {
    ADS_FUNC_BEGIN
    if (area == CenterDropArea) {
        dock->addTab(std::move(tab));
        ADS_FUNC_RET("Dropped in central area")
        return dock;
    }

    QSplitter*  targetDockSplitter = findParentSplitter(dock);
    DockWidget* newDock            = newDockWidget();

    Qt::Orientation orientation = (area == TopDropArea
                                   || area == BottomDropArea)
                                      ? Qt::Vertical
                                      : Qt::Horizontal;


    int dockSize = (orientation == Qt::Vertical) ? dock->height()
                                                 : dock->width();

    ADS_LOG << "Initial widget size:" << dockSize;


    if (targetDockSplitter->orientation() == orientation) {
        ADS_LOG << "Orientation is the same as splitter";
        int index = targetDockSplitter->indexOf(dock);

        QList<int> splitterSizes = targetDockSplitter->sizes();
        splitterSizes[index]     = dockSize / 2;
        splitterSizes.insert(splitterSizes.begin() + index, dockSize / 2);

        switch (area) {
            case TopDropArea: {
                ADS_LOG << "TopDropArea";
                targetDockSplitter->insertWidget(index, newDock);
            } break;
            case BottomDropArea: {
                ADS_LOG << "BottomDropArea";
                targetDockSplitter->insertWidget(index + 1, newDock);
            } break;
            case RightDropArea: {
                ADS_LOG << "RightDropArea";
                targetDockSplitter->insertWidget(index + 1, newDock);
            } break;
            case LeftDropArea: {
                ADS_LOG << "LeftDropArea";
                targetDockSplitter->insertWidget(index, newDock);
            } break;
            default: break;
        }

        targetDockSplitter->setSizes(splitterSizes);
    } else {
        ADS_LOG << "Orientation is different from splitter";
        int        index       = targetDockSplitter->indexOf(dock);
        QSplitter* newSplitter = createSplitter(orientation);
        switch (area) {
            case TopDropArea: {
                ADS_LOG << "TopDropArea";
                newSplitter->addWidget(newDock);
                newSplitter->addWidget(dock);
                newSplitter->setSizes({dockSize / 2, dockSize / 2});
                targetDockSplitter->insertWidget(index, newSplitter);
            } break;
            case BottomDropArea: {
                ADS_LOG << "BottomDropArea";
                newSplitter->addWidget(dock);
                newSplitter->addWidget(newDock);
                newSplitter->setSizes({dockSize / 2, dockSize / 2});
                targetDockSplitter->insertWidget(index, newSplitter);
            } break;
            case RightDropArea: {
                ADS_LOG << "RightDropArea";
                newSplitter->addWidget(dock);
                newSplitter->addWidget(newDock);
                newSplitter->setSizes({dockSize / 2, dockSize / 2});
                targetDockSplitter->insertWidget(index, newSplitter);
            } break;
            case LeftDropArea: {
                ADS_LOG << "LeftDropArea";
                newSplitter->addWidget(newDock);
                newSplitter->addWidget(dock);
                targetDockSplitter->insertWidget(index, newSplitter);
                newSplitter->setSizes({dockSize / 2, dockSize / 2});
            } break;
            default: break;
        }
    }

    newDock->addTab(std::move(tab));
    ADS_FUNC_END
    return newDock;
}


DockWidget* DockManager::dockAt(const QPoint& localPos) const {
    QPoint globalPos = mapToGlobal(localPos);
    return spt::safe_uptr_get(
        docks, [&](const std::unique_ptr<DockWidget>& dock) {
            if (dock == nullptr) {
                return false;
            }
            return dock->rect().contains(dock->mapFromGlobal(globalPos));
        });
}

DockWidget* DockManager::dropContentOuterHelper(
    std::unique_ptr<Tab> tab,
    Qt::Orientation      orientation,
    bool                 append) {
    ADS_FUNC_BEGIN


    DockWidget* dock = newDockWidget();
    dock->addTab(std::move(tab));

    QSplitter* oldsp = findImmediateSplitter(this);
    if (!oldsp) {
        QSplitter* sp = createSplitter(orientation);
        if (mainLayout->count() > 0) {
            qWarning()
                << "Still items in layout. This should never happen.";
            QLayoutItem* li = mainLayout->takeAt(0);
            delete li;
        }
        mainLayout->addWidget(sp);
        sp->addWidget(dock);
    } else if (
        oldsp->orientation() == orientation || oldsp->count() == 1) {
        oldsp->setOrientation(orientation);
        if (append)
            oldsp->addWidget(dock);
        else
            oldsp->insertWidget(0, dock);
    } else {
        QSplitter* sp = createSplitter(orientation);
        if (append) {
            QLayoutItem* li = mainLayout->replaceWidget(oldsp, sp);
            sp->addWidget(oldsp);
            sp->addWidget(dock);
            delete li;

        } else {
            sp->addWidget(dock);
            QLayoutItem* li = mainLayout->replaceWidget(oldsp, sp);
            sp->addWidget(oldsp);
            delete li;
        }
    }

    ADS_FUNC_END
    return dock;
}

void DockManager::onActiveTabChanged() {
    TabTitle* stw = qobject_cast<TabTitle*>(sender());
    if (stw) {
        emit activeTabChanged(stw->getTab(), stw->isActiveTab());
    }
}

DockWidget* DockManager::findTabInDocks(Tab* const tabContent) {
    return spt::safe_uptr_get(
        docks, [&](const std::unique_ptr<DockWidget>& dock) {
            return 0 < dock->indexOfTab(tabContent);
        });
}


void DockManager::setCloseable(bool value) {
    closeable = value;
}

DropOverlay* DockManager::getDropOverlay() {
    return dropOverlay;
}


} // namespace ads
