#pragma once

#include <QPoint>
#include <QPointer>
#include <QWidget>
#include <memory>

#include "support.hpp"
//#include "tab.hpp"

namespace ads {
class DockManager;
class DockWidget;
class FloatingWidget;
class Tab;
} // namespace ads

namespace ads {
/*!
 * \brief Widget that will be shown in tab's title
 */
class TabTitle : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(bool activeTab READ isActiveTab WRITE setActiveTab NOTIFY
                                                                  activeTabChanged)

    /// Parent content for this title
    Tab* tab = nullptr;

    /// Floating widget that this tab is attached to
    FloatingWidget* floating = nullptr;
    QPoint          dragStartPos;

    bool tabMoving = false;
    bool activeTab = false;

    QWidget* tabWidget;

  public:
    TabTitle(std::unique_ptr<QWidget> widget, Tab* _tab);
    virtual ~TabTitle() override; /// \todo Do we need it?

    bool            isActiveTab() const;
    void            setActiveTab(bool active);
    FloatingWidget* getFloating() const;
    void            setFloating(FloatingWidget* value);

    Tab* getTab() const;

  protected:
    void initUI();

    void mousePressEvent(QMouseEvent* ev) override;
    void mouseReleaseEvent(QMouseEvent* ev) override;
    void mouseMoveEvent(QMouseEvent* ev) override;

    int dragStartThreshold;

    // Tab movement
    void moveFloatingWidget(QMouseEvent* ev);
    void beginFloatingWidget(QMouseEvent* ev);
    void moveTab(QMouseEvent* ev);
    bool tabTearOff(QMouseEvent* ev);
    bool movingFloating(QMouseEvent* ev);
    bool startMovingTab(QMouseEvent* ev);
    bool continueTabMove();
    bool movedTab();

    // Dropping widgets
    void     dropOntoContainer(QMouseEvent* ev);
    void     dropOntoSideArea(QMouseEvent* ev);
    bool     overSideArea(QMouseEvent* ev);
    DropArea areaUnderMouse(QMouseEvent* ev);


  signals:
    void activeTabChanged();
    void clicked();
};

} // namespace ads
