#ifndef SECTION_CONTENT_WIDGET_H
#define SECTION_CONTENT_WIDGET_H

#include <QWidget>
#include <memory>

namespace ads {
class Tab;
}

namespace ads {
/*!
 * \brief Class-wrapper that contans widget inside tab's main area
 */
class TabWidget : public QWidget
{
    Q_OBJECT
  public:
    TabWidget(std::unique_ptr<QWidget> _widget, Tab* _tab);
    QWidget* getWidget() const;

  private:
    Tab*     tab;
    QWidget* widget;
};

} // namespace ads
#endif
