QT += core gui widgets xml

TARGET = qt_docking_system
TEMPLATE = lib

CONFIG += console
QMAKE_CXXFLAGS += -std=c++17
CONFIG += staticlib

include($$EXTERNAL_DIR/DebugMacro/debug.pri)
include($$EXTERNAL_DIR/Algorithm/algorithm.pri)
include($$EXTERNAL_DIR/Support/support.pri)
include($$PWD/docking_system/docking_system.pro)
